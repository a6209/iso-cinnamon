#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- == *i* ]] && source /usr/share/blesh/ble.sh --noattach
[[ ${BLE_VERSION-} ]] && ble-attach
alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
pfetch
motivate
eval "$(starship init bash)"